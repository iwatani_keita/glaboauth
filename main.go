package main

import (
	"encoding/json"
	"errors"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Config struct {
	Port        string
	DbHost      string
	ClientID    string
	AppSecret   string
	RedirectURI string
}

func main() {
	config := get_config()
	r := gin.Default()
	// Cors
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return true
		},
		MaxAge: 12 * time.Hour,
	}))
	// Routing
	r.POST("/oauth", oauth)
	r.Run(":" + config.Port)
}

func get_config() Config {
	return Config{
		Port:        "8000",
		ClientID:    "xxxxx",
		AppSecret:   "xxxxx",
		RedirectURI: "http://127.0.0.1:3000/auth",
	}
}

// クライアント側で認証を許可すると呼ばれるエンドポイント
func oauth(c *gin.Context) {
	c.Request.ParseForm()
	// callback urlに併せて付いてくるcodeとstateを使ってパラメータを組み立てる
	code := strings.Join(c.Request.Form["code"], "")
	state := strings.Join(c.Request.Form["state"], "")
	oauth, err := oauth_token(code, state)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	// 認証したユーザでAPIをコールする
	user, err := get_user(oauth.AccessToken)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(200, user)
}

func oauth_token(code string, state string) (OAuth, error) {
	config := get_config()
	endpoint := "https://gitlab.com/oauth/token"

	// HTTPクライアントを作成
	client := &http.Client{}
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}
	values := url.Values{}
	// callback urlに併せて付いてくるcodeとstateを使ってパラメータを組み立てる
	values.Add("client_id", config.ClientID)
	values.Add("client_secret", config.AppSecret)
	values.Add("code", code)
	values.Add("state", state)
	values.Add("grant_type", "authorization_code")
	values.Add("redirect_uri", config.RedirectURI)

	// HTTPリクエストを作成
	req, err := http.NewRequest("POST", endpoint, strings.NewReader(values.Encode()))
	if err != nil {
		return OAuth{}, errors.New(err.Error())
	}

	// リクエストを実行
	resp, err := client.Do(req)
	if err != nil {
		return OAuth{}, errors.New(err.Error())
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return OAuth{}, errors.New(err.Error())
	}
	// JSONをパース
	var oauth OAuth
	if err := json.Unmarshal(body, &oauth); err != nil {
		return OAuth{}, errors.New(err.Error())
	}
	return oauth, nil
}

type OAuth struct {
	AccessToken string `json:"access_token"`
}

type User struct {
	Id        int    `json:"id"`
	Username  string `json:"username"`
	Name      string `json:"name"`
	AvatarUrl string `json:"avatar_url"`
}

// 取得したトークンでユーザ情報を取得
func get_user(token string) (User, error) {
	endpoint := "https://gitlab.com/api/v4/user"

	// HTTPクライアントを作成
	client := &http.Client{}
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}

	// HTTPリクエストを作成
	req, err := http.NewRequest("GET", endpoint, nil)

	// Bearerトークンを指定
	req.Header.Add("Authorization", "Bearer "+token)
	if err != nil {
		return User{}, errors.New(err.Error())
	}

	// リクエスト実行
	resp, err := client.Do(req)
	if err != nil {
		return User{}, errors.New(err.Error())
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return User{}, errors.New(err.Error())
	}

	// JSONをパース
	var user User
	if err := json.Unmarshal(body, &user); err != nil {
		log.Fatal(err)
	}
	return user, nil
}
